from django.urls import path

from .views import(
    api_list_packages,
    api_show_package,
    api_list_questions,
    api_show_question,
)

urlpatterns = [
    path("packages/", api_list_packages, name="api_list_packages"),
    path("package/<int:pk>/", api_show_package, name="api_show_package"),
    path("questions/", api_list_questions, name="api_list_questions"),
    path("question/<int:pk>/", api_show_question, name="api_show_question"),
]
