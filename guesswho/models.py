from django.db import models
from django.urls import reverse

# Create your models here.

class Package(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_package", kwargs={"pk": self.id})

class Question(models.Model):
    name = models.CharField(max_length=250)
    picture_url = models.URLField()
    package = models.ForeignKey(
        Package,
        related_name="questions",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_question", kwargs={"pk": self.id})
