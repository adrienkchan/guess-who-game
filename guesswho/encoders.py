from .json import ModelEncoder
from .models import Package, Question


class PackageEncoder(ModelEncoder):
    model = Package
    properties = [
        "id",
        "name",
    ]

class QuestionEncoder(ModelEncoder):
    model = Question
    properties = [
        "id",
        "name",
        "picture_url",
        "package",
    ]
    encoders = {
        "package": PackageEncoder(),
    }
