from django.contrib import admin
from .models import Question, Package

# Register your models here.

admin.site.register(Package)
admin.site.register(Question)
