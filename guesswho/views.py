from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Package, Question
from .encoders import PackageEncoder, QuestionEncoder

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_packages(request):
    if request.method == "GET":
        packages = Package.objects.all()
        return JsonResponse(
            {"packages": packages},
            encoder=PackageEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            package = Package.objects.create(**content)
            return JsonResponse(
                package,
                encoder=PackageEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create package"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_package(request, pk):
    if request.method == "GET":
        try:
            package = Package.objects.get(id=pk)
            return JsonResponse(
                package,
                encoder=PackageEncoder,
                safe=False,
            )
        except Package.DoesNotExist:
            return JsonResponse({"message": "Package does not exist"})
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            package = Package.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(package, prop, content[prop])
            package.save()
            return JsonResponse(
                package,
                encoder=PackageEncoder,
                safe=False,
            )
        except Package.DoesNotExist:
            return JsonResponse({"message": "Package does not exist"})
    else: #DELETE
        try:
            package = Package.objects.get(id=pk)
            package.delete()
            return JsonResponse(
                package,
                encoder=PackageEncoder,
                safe=False,
            )
        except Package.DoesNotExist:
            return JsonResponse({"message": "Package does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_questions(request):
    if request.method == "GET":
        questions = Question.objects.all()
        return JsonResponse(
            {"questions": questions},
            encoder=QuestionEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            package_id = content["package"]
            package = Package.objects.get(id=package_id)
            content["package"] = package
            question = Question.objects.create(**content)
            return JsonResponse(
                question,
                encoder=QuestionEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Unable to create question"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_question(request, pk):
    if request.method == "GET":
        try:
            question = Question.objects.get(id=pk)
            return JsonResponse(
                question,
                encoder=QuestionEncoder,
                safe=False,
            )
        except Question.DoesNotExist:
            return JsonResponse({"message": "Question does not exist"})
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            question = Question.objects.get(id=pk)

            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(question, prop, content[prop])
            question.save()
            return JsonResponse(
                question,
                encoder=QuestionEncoder,
                safe=False,
            )
        except Question.DoesNotExist:
            return JsonResponse({"message": "Question does not exist"})
    else: #DELETE
        try:
            question = Question.objects.get(id=pk)
            question.delete()
            return JsonResponse(
                question,
                encoder=QuestionEncoder,
                safe=False,
            )
        except Question.DoesNotExist:
            return JsonResponse({"message": "Question does not exist"})
